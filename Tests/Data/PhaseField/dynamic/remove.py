#!/usr/bin/env python2

# coding: utf-8

#Usage pvpython ~/scripts/remove_2D.py 10 ex1_.vtu ex1.vtu
# Tria = 5
# Quad = 9
# Tet = 10
# Hex = 12
# piramid = 14
from vtk import *
import sys

if len(sys.argv) != 3:
    print("Usage:", sys.argv[0], "cell_type input.vtu output.vtu")
#    exit(1)

# r = vtkXMLUnstructuredGridReader()
r = vtkUnstructuredGridReader()
r.SetFileName(sys.argv[4])
r.Update()
m = r.GetOutput()
ct = m.GetCellTypesArray()

print("Mesh's cell types:")
current_type = None
for i in range(ct.GetNumberOfTuples()):
    if ct.GetValue(i) == current_type:
        continue
    current_type = ct.GetValue(i)
    print(current_type)

cell_type_to_select1 = int(sys.argv[1])
cell_type_to_select2 = int(sys.argv[2])
cell_type_to_select3 = int(sys.argv[3])
indices = vtkIdTypeArray()
indices.SetNumberOfComponents(1)
for i in range(ct.GetNumberOfTuples()):
    if ct.GetValue(i) == cell_type_to_select1 or ct.GetValue(i) == cell_type_to_select2 or ct.GetValue(i) == cell_type_to_select3:
        indices.InsertNextValue(i)

selection_node = vtkSelectionNode
selection_node = vtkSelectionNode()
selection_node.SetFieldType(vtkSelectionNode.CELL)
selection_node.SetContentType(vtkSelectionNode.INDICES)
selection_node.SetSelectionList(indices)

selection = vtkSelection()
selection.AddNode(selection_node)
extract_selection = vtkExtractSelection()
extract_selection.SetInputConnection(0, r.GetOutputPort())
extract_selection.SetInputData(1, selection)
extract_selection.Update()

w = vtkXMLUnstructuredGridWriter()
w.SetFileName(sys.argv[5])
w.SetInputConnection(extract_selection.GetOutputPort())
w.Update()
