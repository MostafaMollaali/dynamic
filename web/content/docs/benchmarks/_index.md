+++
title = "Benchmarks"
layout = "subsections"

[cascade]
breadcrumbs = true

[menu.docs]
name = "Benchmarks"
identifier = "benchmarks"
weight = 3
url = "./benchmarks"
post = "Basic benchmarks are explained and input files are provided to get you started in using OGS."
+++
